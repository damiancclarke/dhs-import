import os
import urllib2
import zipfile
from urllib2 import urlopen, URLError, HTTPError
import re
import webbrowser

h1 = 'http://www.measuredhs.com/data/dataset/'
h2 = '_Standard-DHS_'
h3= '.cfm?flag=1'
data1 = 'http://www.measuredhs.com/customcf/legacy/data/download_dataset.cfm?Filename='
data2 = '&Tp=1&Ctry_Code='


url = 'http://www.measuredhs.com/customcf/legacy/data/download_dataset.cfm?Filename=BDBR51DT.ZIP&Tp=1&Ctry_Code=BD'
webbrowser.open(url)
#testoutput = open("test", "wb")
#testoutput.write(test)
#testoutput.close()

for year in range(1990,2012):
	for line in open("DHS_Countries.txt"):
		print(line[0:-1])
		country_code=line[0:2]
		print(country_code)
		url = h1 + line[3:-1] + h2 + str(year) + h3
		print(url)
		response = urllib2.urlopen(url).read()
		matches = re.findall('The link or bookmark is no longer valid', response);
		if len(matches) == 0: 
			print('% s has a DHS survey in %d' %(line[0:-1], year))
			survey = response.find('dt.zip');
			while survey > -1:
				print "dt.zip found at location", survey
				survey_name = response[survey-10:survey+10]
				surveynamelow = survey_name.strip()
				survey_name = surveynamelow.upper()
				print(survey_name)
				urldata = data1 + survey_name + data2 + country_code
				print "fetching url data: %s\n" % urldata 
				webbrowser.open(urldata)
				survey=response.find('dt.zip', survey+1)

#				zippeddata = responsedat.read()
#				output = open(os.path.basename(surveynamelow), "wb")
#				output.write(zippeddata)
				
#				output.close()

#				responsedat = urllib2.urlopen(urldata).read()
#				name = country_code + str(year) + survey_name
#				output = open(name, "wb")
#				output.write(responsedat)
#				output.close()

				
		
		else:
			print('There is no DHS survey in %s in %d' %(line[0:-1], year))


