#  DHS_Import 1.01              damiancclarke             yyyy-mm-dd:2012-09-01
#---|----1----|----2----|----3----|----4----|----5----|----6----|----7----|----8
#
# Syntax is: python DHS_Import dir1 dir2
#  where dir1 is the file where unzipped files should go, and dir2 the downloads
#  file on the computer.
#  In my case this looks something like:
#         python DHS_Import.py ~/database/DHS/DHS_Data ~/Descargas
#
# This file downloads all publicly available DHS surveys up to the moment that
# the script is run, however does not download continuous surveys (ie Peru 2004-
# 2006).  If you GIS surveys should be downloaded the variable GEO should be set
# equal to 1 (as is currently the case on line 33).
#
# When running this script two requirements must be met: firstly permission must
# be granted for you to download the DHS surveys (simply apply at measuredhs.com)
# and secondly you must be signed into your DHS account.  This should be done at:
# https://dhsprogram.com/data/dataset_admin
#
# For optimal viewing of this code, set tab width to 2 characters.
#
# Contact: damian.clarke@economics.ox.ac.uk

#*******************************************************************************
# (1a) Import required packages, open files to write download countries
#*******************************************************************************
from sys import argv
import os
import urllib, urllib2
from urllib2 import urlopen, URLError, HTTPError
import re
import webbrowser, cookielib
import zipfile

GEO = 1

script, filename1, filename2 = argv

f_surveys = open("DHS_Surveys.txt", "w")
f_surveys_stata = open("DHS_Surveys_StataUse.txt", "w")
f_countries = open("DHS_SurveyCountry.txt", "w")
f_errors = open("exceptedCases.txt", "w")

#*******************************************************************************
# (1b) Pieces of URL which allow us to piece together unique locations of files
#*******************************************************************************
h1 = 'http://www.measuredhs.com/data/dataset/'
h2 = ['_Standard-DHS_', '_Special_', '_Standard-AIS_']
h2d = '_Continuous-DHS_'
h3= '.cfm?flag=1'

data1 = 'http://www.measuredhs.com/customcf/legacy/data/download_dataset.cfm?Filename='
data2 = '&Tp=1&Ctry_Code='

geo_d = '&Tp=2&Ctry_Code='

#*******************************************************************************
# (2) download zip files for each survey
#     Search for urls which match Stata data files for IR/PR/BR and so on
#     In the case that GIS data is required, also search for and download GIS
#
#*******************************************************************************
for year in range(2002,2014):
	for line in open("DHS_Countries.txt"):
		country = line[0:-1]
		country_name = line[3:-1]
		print country + " " + str(year)
		country_code=line[0:2]

		for n in h2:
			url = h1 + line[3:-1] + n + str(year) + h3
			print(url)
			try:
				response = urllib2.urlopen(url).read()
			except:
				print('%s did not work...' %url)
				print 'Perhaps a URL error or connection issue'
				f_errors.write(url + "\t" + country + "\t" + str(year) + "\n")
				break

			matches = re.findall('The link or bookmark is no longer valid', response);
			if len(matches)==0:
				break

		#if there is no match for missing, the country has a survey
		if len(matches) == 0: 
			print('%s has a DHS survey in %d' %(line[0:-1], year))
			f_countries.write(country + "\t" + str(year) + "\n")

			searchterm = ['dt.zip']
			if GEO==1:
				searchterm = [country_code.lower() + 'ge', 'dt.zip']

			for st in searchterm:
				survey = response.find(st);
				while survey > -1:
					print "searchterm found at location", survey
					if st=='dt.zip':
						survey_name = response[survey-6:survey+6]
					else:
						survey_name = response[survey:survey+12]
						print survey_name
						zipmatch = re.search('\.zip$',survey_name)
						if not zipmatch:
							f_errors.write(survey_name)
							break
					surveynamelow = survey_name.strip()
					survey_name = surveynamelow.upper()
					print(survey_name)
					survey_s = survey_name[0:-4]
					recode = survey_name[2:4]
					if st=='dt.zip':
						urldata = data1 + survey_name + data2 + country_code
					else:
						urldata = data1 + survey_name + geo_d + country_code						
					print "fetching url data: %s\n" % urldata 
					try:
						webbrowser.get('firefox').open(urldata)
					except:
						print('%s did not work...' %urldata)
						print 'Perhaps a URL error or connection issue'
						f_errors.write(urldata + "\t" + country + "\t" + str(year) + "\n")

				# Create file listing all DHS countries, years, and the surveys
					f_surveys.write(country + "\t" + str(year) + "\t" + survey_name + "\n")
					f_surveys_stata.write(country_code + "\n" + country_name + "\n")
					f_surveys_stata.write(str(year) + "\n" + survey_s + "\n" + recode + "\n")
				# skip to next file type for given country and year
					survey=response.find(st, survey+1)
		
		else:
			print('There is no DHS survey in %s in %d' %(line[0:-1], year))

f_surveys.close()
f_surveys_stata.close()
f_countries.close()
f_errors.close()

#*******************************************************************************
# (3) Unzip folders saving .dta files in appropriate folders
#*******************************************************************************
cwd = os.getcwd()
for line in open("DHS_SurveyCountry.txt"):
	#Create folders with names of countries and years (note that here lines
	#could be removed using makedirs instead of mkdir)
	os.chdir(argv[1])	
	country = line[3:-6]
	year = line[-5:-1]
	if not os.path.isdir(country):
		os.mkdir(country)
		os.chdir(country)
		os.mkdir(year)
	if os.path.isdir(country):
		os.chdir(country)
		if not os.path.exists(year):
			os.mkdir(year)
	os.chdir(cwd)

for line in open("DHS_Surveys.txt"):
	#Record country, year, survey name, survey type, and file name
	country = line[3:-19]
	year = line[-18:-14]
	survey = line[-13:-1]
	survey_save = survey[0:-4]
	recode = line[-11:-9]
	os.chdir(cwd)
	os.chdir(argv[2])

	if os.path.isfile(survey):
		#Read in compressed zip file, uncompress, and save .dta files:
		zin = zipfile.ZipFile(survey, "r")
		for info in zin.infolist():
			fname1 = info.filename
			fname = fname1[-4:]
			fname_save = fname1[0:-5]
			print fname
			if fname == ".dta" or fname == ".DTA":
				print fname
				#read in .dta files only
				data = zin.read(fname1)
				os.chdir(cwd)
				os.chdir(argv[1]+'/'+country+'/'+year)
				#Save decompressed stata files in appropriate folders

				fout = open(survey_save + ".dta", "w")
				fout.write(data)
				fout.close()
			elif recode == "GE":
				print country + year
				data = zin.read(fname1)
				os.chdir(cwd)
				os.chdir(argv[1]+'/'+country+'/'+year)
				if not os.path.exists('Geo'):
					os.mkdir('Geo')
				os.chdir('Geo')
				print("fname1 is: %s" %fname1)
				fout = open(fname1, "w")
				fout.write(data)
				fout.close()


			os.chdir(cwd)
			os.chdir(argv[2])
