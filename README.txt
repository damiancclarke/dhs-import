================================================================================
README.txt for DHS Import
Contact: damian.clarke@econommics.ox.ac.uk
Last version: 1.0.1
     > Incorporated option to download GIS data into original code
================================================================================

This folder contains code to download all DHS data from measuredhs.com, and 
merge into one 'world wide' DHS file.  The principal work is done in two scripts:
      > DHS_Import.py
	    - Downloads each file from the web and saves on your machine
      > DHS_Multicountry.do
	    - Appends all country-specific recodes together into on worldwide 
	      file

The scripts also require one information file.  This is "DHS_Countries.txt" 
which lists the full set of DHS countries for which you wish to download files.
By default, it is assumed you want to download all files, however if this is not
the case the txt file should be edited to reflect this.

Details regarding how to run each of DHS_Import.py (the download script), and 
DHS_Multicountry.do (the munging script) are available in the headers of each
file.
